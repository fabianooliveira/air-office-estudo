import React from 'react';
import {
    View,
    ImageBackground,
    ScrollView,
    StyleSheet,
    Image,
    Alert
} from 'react-native';

import { Formik } from 'formik';
import * as yup from 'yup';

import { Input, Button, colors, ListItem } from 'react-native-elements';
import Logo from '../../assets/white-logo.png'; // .. volta // .. volta de novo // entra na pasta assets // procura white - logo
import BgImage from '../../assets/login-bg.png'; // .. volta // .. volta de novo // entra na pasta assets // procura white - logo
import WhiteButton from '../../components/WhiteButton';
import { white } from 'ansi-colors';
const FAKE_DATA = {
    email: 'fabianooliveirale@gmail.com',
    password: '123456',
}

export default class LoginForm extends React.Component {
    render() {
        return (
            <Formik
                initialValues={FAKE_DATA}
                onSubmit={this._submit}
            >
                {form => (
                    <ImageBackground source={BgImage} style={css.background}>
                        <ScrollView contentContainerStyle={css.scroll}>
                            <View style={css.logoContainer}>
                                <Image source={Logo} style={css.logo} />
                            </View>
                            <Input
                                errorMessage={form.errors.email}
                                value={form.values.email}
                                inputStyle = {css.labelStyleColor}
                                containerStyle={css.inputStyle}
                                onChangeText={form.handleChange('email')}
                                placeholder="E-mail" />
                            <Input
                                errorMessage={form.errors.password}
                                containerStyle={css.inputStyle}
                                inputStyle = {css.labelStyleColor}
                                value={form.values.password}
                                onChangeText={form.handleChange('password')}
                                placeholder="Senha"></Input>
                            <WhiteButton
                                style={{ alignSelf: 'flex-end' }}
                                title="Esqueceu sua senha?"></WhiteButton>
                            <View style={[css.row, { alignSelf: 'center' }]}>
                                <WhiteButton title="face"></WhiteButton>
                                <WhiteButton title="in"></WhiteButton>
                            </View>
                            <Button
                                title="Login"
                                containerStyle={css.buttonStyle}
                                onPress={form.handleSubmit}
                                loading={form.isSubmitting}
                            />
                            <WhiteButton title="Cadastre-se"
                                style={{ alignSelf: 'flex-end' }}
                                onPress={() => {
                                    this.props.navigation.navigate('Signup')
                                }}></WhiteButton>
                            <WhiteButton title="Cadastre-se DOIS"
                                style={{ alignSelf: 'flex-end' }}
                                onPress={() => {
                                    //this.props.navigation.navigate('SignupTwo')
                                }}></WhiteButton>
                        </ScrollView>
                    </ImageBackground>
                )}
            </Formik>
        );
    }

    _submit = (values, actions) => {
        FormDefaultSchema.validate(values, { abortEarly: false })
            .then(valid => {
                if (valid) this.props.onSubmit(values).then(() => {
                    actions.setSubmitting(false);
                })
            })
            .catch(formErrors => {
                const errors = {};
                formErrors.inner &&
                    formErrors.inner.forEach(err => {
                        errors[err.path] = err.message;
                    });
                Alert.alert(
                    'Não foi possível continuar',
                    'Verifique os erros e tente novamente'
                );

                actions.setSubmitting(false);
                actions.setErrors(errors);
            });
    };
}

const css = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        backgroundColor: '#262626'
    },
    scroll: {
        flexGrow: 1,
    },
    logo: {
        width: '45%',
        height: 60,
        resizeMode: 'contain'
    },
    logoContainer: {
        paddingVertical: 90,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row',
    },
    inputStyle: {
        paddingLeft: 18,
        paddingRight: 18,
    },
    buttonStyle: {
        marginLeft: 18,
        marginRight: 18
    },
    labelStyleColor: {
        color: 'white',
    },
});

yup.setLocale({
    mixed: {
        default: 'Não é válido',
        required: 'deve ser preenchido',
    },
    string: {
        email: 'deve ser um email válido!',
    },
});

const FormDefaultSchema = yup.object().shape({
    password: yup.string().required(),
    email: yup.string().email().required(),
});