import React from 'react';
import gql from 'graphql-tag';
import { Alert,AsyncStorage } from 'react-native';
import { Mutation } from 'react-apollo';

import LoginForm from './LoginForm';
import Server from '../../constants/Server';

const SignupMutation = gql`
  mutation officeRenterLogin($input: OfficeRenterLoginInput!) {
    officeRenterLogin(input: $input) {
      errors
      token {
        accessToken
      }
    }
  }
`;

export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
};
  render() {
    return (
      <Mutation mutation={SignupMutation}>
        {mutate => (
          <LoginForm
            onSubmit={values => this._tryToSignup(mutate, values)}
            navigation = {this.props.navigation}
          />
        )}
      </Mutation>
    );
  }

  _tryToSignup = (mutate, formValues) => {
    console.log(formValues)
    const input = {
      ...formValues,
      clientId: Server.clientId,
      clientSecret: Server.clientSecret,
    };

    return mutate({ variables: { input } })
      .then(r => {
        if (r.data.officeRenterLogin.errors != null){
          Alert.alert("Alerta!",r.data.officeRenterLogin.errors[0]);
          return
        }
        AsyncStorage.setItem("AccessToken", r.data.officeRenterLogin.token.accessToken)
        this.props.navigation.navigate('Loading')
      })
      .catch(err => {
        console.log(err.message);
        Alert.alert(err.message);
      });
  };
}