import React from 'react';
import {
    ImageBackground,
    ScrollView,
    StyleSheet,
} from 'react-native';

import BgImage from '../../assets/login-bg.png'; // .. volta // .. volta de novo // entra na pasta assets // procura white - logo

export default class ChatScreen extends React.Component {
    static navigationOptions = {
        title: 'Upload de Fotos',
        headerStyle: {
            backgroundColor: '#0a5a8e',
        },
        headerTintColor: 'white',
    };
    render() {
        return (
            <ImageBackground source={BgImage} style={css.background}>
                <ScrollView contentContainerStyle={css.scroll}>
                   
                </ScrollView>
            </ImageBackground>
        );
    }

   
}

const css = StyleSheet.create({
    background: { // justifica em coluna
        backgroundColor: '#262626',
        width: '100%',
        height: '100%',
        flex: 1
        // justifica em row
    },
    scroll: {
        flexGrow: 1,
    },
    logoContainer: {
        paddingVertical: 50,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    buttonTop: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 110,
        marginLeft: 18,
        marginRight: 18
    },
    button: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 20,
        marginLeft: 18,
        marginRight: 18
    },
    textStyleButton: {
        fontSize: 20,
        color: 'black',
    },
});
