import React from 'react';
import {
    StyleSheet,
    ActivityIndicator,
    Text,
    View,
    Image,
} from 'react-native';


import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import MapView from 'react-native-maps';
import { Marker, Callout } from 'react-native-maps';
import { Svg, G, Path } from 'react-native-svg';
import { Rating } from 'react-native-elements';

const SearchQuery = gql`
  query MobileOfficesMapSearchQuery {
    offices(first: 50) {
      nodes {
        id
        name
        avgRating
        mainImage {
          id
          variant(resize: "100x100")
        }
        periodPrice
        availablePeriodsDescription

        address {
          id
          lat
          lng
        }
      }
    }
  }
`;

const Icon = () => (
    <Svg width={45} height={63} viewBox="0 0 45 63">
        <G fill="none" fill-rule="evenodd">
            <G fill="#0A5A8E">
                <Path
                    d="M21.503 62.466c-.41-.258 18.591-9.514 21.7-15.833 3.11-6.32.617-13.909-5.567-16.952-6.185-3.043-13.719-.387-16.828 5.932-3.11 6.319 1.105 27.11.695 26.853z"
                    opacity=".185"
                />
                <Path
                    stroke="#FFF"
                    d="M21.903 61.273c-.44 0 20.903-27.622 20.903-39.239C42.806 10.417 33.448 1 21.903 1 10.36 1 1 10.417 1 22.034c0 11.617 21.343 39.239 20.903 39.239z"
                />
            </G>
            <Path
                fill="#FFF"
                d="M19.397 31h-2.746l-.86-3.939h-3.374L11.599 31H9l3.438-15.066h3.249L19.397 31zm-4.15-6.457h-2.285l1.09-4.65 1.195 4.65zm9.5 6.457h-2.305V20.217h2.306V31zm6.69 0H29.13V20.217h2.138l.167.086c.462-.158.93-.244 1.405-.258h.42c.265 0 .46.007.586.021.196.029.58.108 1.153.237l-.545 2.303c-.391-.201-.769-.302-1.132-.302-.307 0-.608.065-.901.194-.657.302-.986.825-.986 1.571V31zm-7.72-12.128c-1.847 0-3.346-1.538-3.346-3.436S21.87 12 23.718 12c1.848 0 3.346 1.538 3.346 3.436s-1.498 3.436-3.346 3.436z"
            />
        </G>
    </Svg>
)

export default class Upload extends React.Component {
    static navigationOptions = {
        title: 'Maps',
        headerStyle: {
            backgroundColor: '#0a5a8e',
        },
        headerTintColor: 'white',
    };

    constructor(props) {
        super(props);
        this.state = {
            region: {
                latitude: -25.440504,
                longitude: -49.282288,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            markers: [{ latlng: { latitude: -25.440504, longitude: -49.282288 }, title: "olá", description: "ahahaha", id: 1 }]
        }
    }

    render() {
        return (
            <Query query={SearchQuery}>
                {resposta => {
                    if (resposta.loading) return <ActivityIndicator />;
                    if (resposta.error) {
                        return (
                            <Text style={{ color: 'red' }}>
                                {resposta.error.message}
                            </Text>
                        );
                    }
                    return <MapView
                        region={this.state.region}
                        style={{ flex: 1 }}
                    >
                        {console.log(resposta.data)}
                        {resposta.data.offices.nodes.map(element => {
                            return <Marker
                                coordinate={{ latitude: element.address.lat, longitude: element.address.lng }}
                                title={element.name}
                                description={element.availablePeriodsDescription}
                                key={element.id}
                            >
                                <Icon></Icon>
                                <Callout>
                                    <View style={css.infoMarker}>
                                        {element.mainImage && (
                                            <Image
                                                style={{ width: 50, height: 50 }}
                                                source={{ uri: element.mainImage.variant }}>
                                            </Image>
                                        )}
                                        <Rating
                                            imageSize={20}
                                            readonly
                                            startingValue={element.avgRating}
                                          />
                                        <Text>AMIGAO</Text>
                                    </View>
                                </Callout>
                            </Marker>
                        })}
                    </MapView>
                }}
            </Query>
        );
    }

    _Console = (data) => {

    }
}

const css = StyleSheet.create({
    background: { // justifica em coluna
        backgroundColor: '#262626',
        width: '100%',
        height: '100%',
        flex: 1
        // justifica em row
    },
    scroll: {
        flexGrow: 1,
    },
    logoContainer: {
        paddingVertical: 50,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    buttonTop: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 110,
        marginLeft: 18,
        marginRight: 18
    },
    button: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 20,
        marginLeft: 18,
        marginRight: 18
    },
    textStyleButton: {
        fontSize: 20,
        color: 'black',
    },
    infoMarker: {
        width: 300,
        height: 120,
    }
});
