import React from 'react';
import {
    ImageBackground,
    ScrollView,
    StyleSheet,
    Text, AsyncStorage, ActivityIndicator, FlatList
} from 'react-native';

import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import { Input, Button, ListItem } from 'react-native-elements';

import BgImage from '../../assets/login-bg.png'; // .. volta // .. volta de novo // entra na pasta assets // procura white - logo

const OfficeListItemFragment = gql`
  fragment OfficeListItem on Office {
    id
    name
    avgRating
    mainImage {
      id
      variant(resize: "100x100")
    }
    periodPrice
    availablePeriodsDescription
  }
`;

const OfficesQuery = gql`
  query MobileOfficesListSearchQuery {
    offices(first: 20) {
      nodes {
        ...OfficeListItem
      }
    }
  }

  ${OfficeListItemFragment}
`;

const list = []

export default class Listar extends React.Component {
    static navigationOptions = {
        title: 'Listar',
        headerStyle: {
            backgroundColor: '#0a5a8e',
        },
        headerTintColor: 'white',
    };

    renderRow = ({ item }) => {
        return (
            <ListItem
                roundAvatar
                title={item.name}
                subtitle={item.subtitle}
                leftAvatar={{ source: { uri: item.mainImage && item.mainImage.variant } }}
                onPress={() => { this._press(item) }}
                bottomDivider
            />
        )
    }
    render() {
        return (
            <ImageBackground source={BgImage} style={css.background}>
                <ScrollView contentContainerStyle={css.scroll}>
                    <Query query={OfficesQuery}>
                        {resposta => {
                            if (resposta.loading) return <ActivityIndicator />;
                            if (resposta.error) {
                                return (
                                    <Text style={{ color: 'red' }}>
                                        {resposta.error.message}
                                    </Text>
                                );
                            }
                            return <FlatList
                                data={resposta.data.offices.nodes}
                                renderItem={this.renderRow}
                                keyExtractor={item => item.id}
                            />
                        }}
                    </Query>
                </ScrollView>
            </ImageBackground>
        );
    }

    _submit = () => {
        AsyncStorage.setItem("AccessToken", "")
        this.props.navigation.navigate('Login')
    };

    _JogoDaVelha = () => {
        this.props.navigation.navigate('JogoVelha')
    };

    _Listar = () => {
        this.props.navigation.navigate('Listar')
    };
}

const css = StyleSheet.create({
    background: { // justifica em coluna
        backgroundColor: '#262626',
        width: '100%',
        height: '100%',
        flex: 1
        // justifica em row
    },
    scroll: {
        flexGrow: 1,
    },
    logoContainer: {
        paddingVertical: 50,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    buttonTop: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 110,
        marginLeft: 18,
        marginRight: 18
    },
    button: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 20,
        marginLeft: 18,
        marginRight: 18
    },
    textStyleButton: {
        fontSize: 20,
        color: 'black',
    },
});
