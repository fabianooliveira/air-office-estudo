import React from 'react';
import {
    View,
    ImageBackground,
    ScrollView,
    StyleSheet,
    Image,
    Alert,
    Text, AsyncStorage
} from 'react-native';

import { Input, Button } from 'react-native-elements';

import { Formik } from 'formik';
import * as yup from 'yup';

import BgImage from '../../assets/login-bg.png'; // .. volta // .. volta de novo // entra na pasta assets // procura white - logo



export default class HomeForm extends React.Component {
    static navigationOptions = {
        header: null,
    };
    render() {
        return (
            <ImageBackground source={BgImage} style={css.background}>
                <ScrollView contentContainerStyle={css.scroll}>
                    <Button
                        buttonStyle={css.buttonTop}
                        titleStyle={css.textStyleButton}
                        title="Upload de Foto"
                        onPress={this._Upload}
                    />
                    <Button
                        buttonStyle={css.button}
                        titleStyle={css.textStyleButton}
                        title="Maps"
                        onPress={this._Maps}
                    />
                    <Button
                        buttonStyle={css.button}
                        titleStyle={css.textStyleButton}
                        title="Listar"
                        onPress={this._Listar}
                    />
                    <Button
                        buttonStyle={css.button}
                        titleStyle={css.textStyleButton}
                        title="Chats"
                        onPress={this._ChatRooms}
                    />
                    <Button
                        buttonStyle={css.button}
                        titleStyle={css.textStyleButton}
                        title="JogoDaVelha"
                        onPress={this._JogoDaVelha}

                    />
                    <Button
                        title="Sair"
                        buttonStyle={css.button}
                        titleStyle={css.textStyleButton}
                        onPress={this._Sair}
                    />
                </ScrollView>
            </ImageBackground>
        );
    }

    _Sair = () => {
        AsyncStorage.setItem("AccessToken", "")
        this.props.navigation.navigate('Login');
    };

    _Maps = () => {
        this.props.navigation.navigate('Map');
    };

    _JogoDaVelha = () => {
        this.props.navigation.navigate('JogoVelha')
    };

    _ChatRooms = () => {
        this.props.navigation.navigate('ChatR')
    };

    _Listar = () => {
        this.props.navigation.navigate('List')
    };

    _Upload = () => {
        this.props.navigation.navigate('Up')
    };
}

const css = StyleSheet.create({
    background: {
        backgroundColor: '#262626',
        width: '100%',
        height: '100%',
    },
    scroll: {
        flexGrow: 1,
    },
    logoContainer: {
        paddingVertical: 50,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    buttonTop: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 40,
        marginLeft: 18,
        marginRight: 18
    },
    button: {
        height: 45,
        borderRadius: 12,
        backgroundColor: 'white',
        marginTop: 20,
        marginLeft: 18,
        marginRight: 18
    },
    textStyleButton: {
        fontSize: 20,
        color: 'black',
    },
});
