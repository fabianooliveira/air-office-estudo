import React from 'react';
import {
    View,
    ImageBackground,
    ScrollView,
    StyleSheet,
    Image,
    Alert,
    Text, AsyncStorage
} from 'react-native';

import { Input, Button } from 'react-native-elements';

import { Formik } from 'formik';
import * as yup from 'yup';

import BgImage from '../../assets/login-bg.png'; // .. volta // .. volta de novo // entra na pasta assets // procura white - logo

class Square extends React.Component {
    render() {
        var desenho = this.props.desenho;
        var id = this.props.idSquare
        return <Button title={desenho} buttonStyle={css.button} onPress={() => { this.props.setarClicado(id) }} titleStyle={css.textStyleButton} />
    }
}

export default class JogoDaVelha extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            x: [],
            o: [],
            proximoTurno: 'X',
            desenho: { 1: " ", 2: " ", 3: " ", 4: " ", 5: " ", 6: " ", 7: " ", 8: " ", 9: " " },
            jogando: true
        }
    }
    static navigationOptions = {
        title: 'Jogo da Velha',
        headerStyle: {
            backgroundColor: '#0a5a8e',
        },
        headerTintColor: 'white',
    };
    render() {
        return (
            <ImageBackground style={css.background}>
                <View style={css.row}>
                    <Square idSquare={1} desenho={this.state.desenho[1]} setarClicado={this._setarClicado} />
                    <Square idSquare={2} desenho={this.state.desenho[2]} setarClicado={this._setarClicado} />
                    <Square idSquare={3} desenho={this.state.desenho[3]} setarClicado={this._setarClicado} />
                </View>
                <View style={css.row}>
                    <Square idSquare={4} desenho={this.state.desenho[4]} setarClicado={this._setarClicado} />
                    <Square idSquare={5} desenho={this.state.desenho[5]} setarClicado={this._setarClicado} />
                    <Square idSquare={6} desenho={this.state.desenho[6]} setarClicado={this._setarClicado} />
                </View>
                <View style={css.row}>
                    <Square idSquare={7} desenho={this.state.desenho[7]} setarClicado={this._setarClicado} />
                    <Square idSquare={8} desenho={this.state.desenho[8]} setarClicado={this._setarClicado} />
                    <Square idSquare={9} desenho={this.state.desenho[9]} setarClicado={this._setarClicado} />
                </View>

            </ImageBackground>
        );
    }

    _setarClicado = (id) => {
        const novoX = this.state.x.slice() // SLICE copia examtamente o objeto para variavel!
        const novoO = this.state.o.slice()

        if (novoX.includes(id) || novoO.includes(id)) {
            return
        }

        const novoDesenho = { ...this.state.desenho }
        novoDesenho[id] = this.state.proximoTurno
        this.setState({ desenho: novoDesenho })

        if (this.state.proximoTurno == 'X') {
            novoX.push(id)
            this.setState({ x: novoX, proximoTurno: 'O' })
        } else {
            novoO.push(id)
            this.setState({ o: novoO, proximoTurno: 'X' })
        }

        if (this._setVictory(this.state.proximoTurno == 'X' ? novoX : novoO)) {
            Alert.alert("( " + this.state.proximoTurno + " )" + " Venceu!");
            this._Reset();
        } else if ((novoX.length + novoO.length) == 9) {
            Alert.alert("Jogo acabou sem vencedor!");
            this._Reset()
        }
    }

    _Reset = () => {
        this.setState({ x: [] })
        this.setState({ o: [] })
        this.setState({ proximoTurno: 'X' })
        this.setState({ desenho: { 1: " ", 2: " ", 3: " ", 4: " ", 5: " ", 6: " ", 7: " ", 8: " ", 9: " " } })
    }

    _setVictory = (list) => {
        var victoryCondition = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]]
        var count = 0;
        var victory = false
        victoryCondition.forEach((condition) => {
            count = 0;
            condition.forEach((item) => {
                if (list.includes(item)) {
                    count = count + 1;
                }
            })
            if (count >= 3) {
                victory = true
            }
        })
        return victory
    }
}

const css = StyleSheet.create({
    background: {
        flex: 3, // divide a tela
        justifyContent: 'center', // justifica em coluna
        alignItems: 'center', // justifica em row
        backgroundColor: '#53535A'
    },
    button: {
        width: 80,
        height: 80,
        margin: 10,
        borderRadius: 12,
        backgroundColor: 'white'
    },
    textStyleButton: {
        fontSize: 40,
        color: 'black',
    },
    row: {
        flexDirection: 'row',
    }
});
