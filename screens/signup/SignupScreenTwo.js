// import React from 'react';
// import * as yup from 'yup';

// import { View, Text, Alert, StyleSheet } from 'react-native';
// import { Button, CheckBox, Input } from 'react-native-elements';
// import { Formik, yupToFormErrors } from 'formik';
// import { KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'


// export default class SignupScreen extends React.Component {
//     static navigationOptions = {
//         title: 'Cadastre-se!',
//         headerStyle: {
//             backgroundColor: '#0a5a8e',
//         },
//         headerTintColor: 'white',
//     };

//     state = {
//         errors: {}
//     };

//     render() {
//         return (
//             <Formik
//                 initialValues={{ kind: 'person' }}
//                 onSubmit={this._submit}
//             >
//                 {form => (
//                     <View>
//                         <KeyboardAwareScrollView>
//                             <View style={css.row}>
//                                 <CheckBox
//                                     title="CPF"
//                                     checked={form.values.kind == 'person'}
//                                     onPress={() => form.handleChange('kind')('person')}
//                                 />
//                                 <CheckBox
//                                     title="CNPJ"
//                                     checked={form.values.kind == 'company'}
//                                     onPress={() => form.handleChange('kind')('company')}
//                                 />
//                             </View>
//                             <Input
//                                 contentContainerStyle={css.marginStyle}
//                                 value={form.values.firstName}
//                                 placeholder="Nome"
//                                 errorMessage={form.errors.firstName}
//                                 onChangeText={form.handleChange('firstName')}
//                             />

//                             <Input
//                                 contentContainerStyle={css.marginStyle}
//                                 value={form.values.lastName}
//                                 placeholder="Sobrenome"
//                                 errorMessage={form.errors.lastName}
//                                 onChangeText={form.handleChange('lastName')}
//                             />

//                             {form.values.kind == 'person' && (
//                                 <Input
//                                     value={form.values.cpf}
//                                     placeholder="CPF"
//                                     errorMessage={form.errors.cpf}
//                                     onChangeText={form.handleChange('cpf')}
//                                 />
//                             )}

//                             {form.values.kind == 'company' && (
//                                 <View>
//                                     <Input
//                                         value={form.values.cnpj}
//                                         placeholder="CNPJ"
//                                         errorMessage={form.errors.cnpj}
//                                         onChangeText={form.handleChange('cnpj')}
//                                     />
//                                     <Input
//                                         value={form.values.companyLegalName}
//                                         placeholder="Razão Social"
//                                         onChangeText={form.handleChange('companyLegalName')}
//                                     />
//                                 </View>
//                             )}

//                             {this._renderPersonFields(form)}
//                             {this._renderCompanyFields(form)}

//                             <Input
//                                 value={form.values.email}
//                                 placeholder="Email"
//                                 style={css.marginStyle}
//                                 errorMessage={form.errors.email}
//                                 onChangeText={form.handleChange('email')}
//                                 keyboardType="email-address"
//                             />

//                             <Input
//                                 value={form.values.password}
//                                 placeholder="Senha"
//                                 style={css.marginStyle}
//                                 errorMessage={form.errors.password}
//                                 onChangeText={form.handleChange('password')}
//                                 secureTextEntry
//                             />

//                             <Input
//                                 value={form.values.passwordConfirmation}
//                                 placeholder="Confirmação de senha"
//                                 style={css.marginStyle}
//                                 errorMessage={form.errors.passwordConfirmation}
//                                 onChangeText={form.handleChange('passwordConfirmation')}
//                                 secureTextEntry
//                             />

//                             <Button
//                                 title="Salvar"
//                                 onPress={form.handleSubmit}
//                                 loading={form.isSubmitting}
//                             />
//                         </KeyboardAwareScrollView>
//                     </View>
//                 )}
//             </Formik>
//         );
//     }

//     _submit = (values, actions) => {
//         console.log('form values', values)
//         const FormSchema = values.kind == 'person' ? FormPersonSchema : FormCompanySchema;
//         FormSchema.validate(values, { abortEarly: false })
//             .then(isValid => {
//                 alert("Valido!")
//             })
//             .catch(formErrors => {
//                 const errors = {};

//                 formErrors.inner &&
//                     formErrors.inner.forEach(err => {
//                         errors[err.path] = err.message;
//                     });

//                 Alert.alert(
//                     'Não foi possível continuar',
//                     'Verifique os erros e tente novamente'
//                 );

//                 console.log(errors)
//                 actions.setSubmitting(false)
//                 actions.setErrors(errors);
//             });
//     };

//     _renderPersonFields = form => {
//         if (form.values.kind == 'company') return;
//         return (
//             <Input
//                 value={form.values.cpf}
//                 placeholder="CPF"
//                 errorMessage={form.errors.cpf}
//                 onChangeText={form.handleChange('cpf')}
//             />
//         );
//     };

//     _renderCompanyFields = form => {
//         if (form.values.kind == 'person') return;
//         return (
//             <View>
//                 <Input
//                     value={form.values.cnpj}
//                     placeholder="CNPJ"
//                     errorMessage={form.errors.cnpj}
//                     onChangeText={form.handleChange('cnpj')}
//                 />
//                 <Input
//                     value={form.values.companyLegalName}
//                     placeholder="Razão Social"
//                     onChangeText={form.handleChange('companyLegalName')}
//                 />
//             </View>
//         );
//     }
// }

// yup.setLocale({
//     mixed: {
//         required: "Preencha o campo!"
//     }
// })

// const FormDefaultSchema = yup.object().shape({
//     kind: yup.string().required(),
//     firstName: yup.string().required(),
//     lastName: yup.string().required(),
//     email: yup.string().required(),
//     password: yup.string().required(),
//     passwordConfirmation: yup.string().required()
// });

// const FormPersonSchema = FormDefaultSchema.concat(
//     yup.object().shape({
//         cpf: yup.string().required(),
//     })
// );

// const FormCompanySchema = FormDefaultSchema.concat(
//     yup.object().shape({
//         cnpj: yup.string().required(),
//     })
// );

// const css = StyleSheet.create({
//     scroll: {
//         flexGrow: 1,
//         alignItems: 'center',
//     },
//     row: {
//         flexDirection: 'row'
//     },
//     marginStyle: {
//         margin: 15,
//         height: 40,
//         borderColor: '#7a42f4',
//         borderWidth: 1
//     }
// });