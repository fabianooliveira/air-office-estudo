import React from 'react';

import { View, Text, Alert, Platform } from 'react-native';
import { Button, CheckBox, Input } from 'react-native-elements';
import { Formik } from 'formik';
import * as yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const FAKE_DATA = {
  avatarUrl: 'https://2.gravatar.com/avatar/767fc9c115a1b989744c755db47feb60?s=132&d=wavatar',
  kind: 'person',
  firstName: 'Rudi',
  lastName: 'francheschi',
  email: 'fabianooliveirale@gmail.com',
  cpf: '070.556.353-56',
  phone: '.p213421',
  password: '123456',
  passwordConfirmation: '123456',
  address: {
    postalCode: '8980112',
    street: 'Rua do deseseperados',
    number: '1234',
    complement: 'e',
    city: 'Cutirina',
    state: 'aoethunato',
  },
  termsOfService: true
}

export default class SignupForm extends React.Component {
  render() {
    return (
      <Formik
        // initialValues={{ kind: 'person' }}
        initialValues={FAKE_DATA}
        onSubmit={this._submit}
      >
        {form => (
          <KeyboardAwareScrollView
            enableOnAndroid
            extraScrollHeight={Platform.select({ android: 200 })}
          >
            <CheckBox
              title="CPF"
              checked={form.values.kind == 'person'}
              onPress={() => form.handleChange('kind')('person')}
            />
            <CheckBox
              title="CNPJ"
              checked={form.values.kind == 'company'}
              onPress={() => form.handleChange('kind')('company')}
            />

            <Input
              value={form.values.firstName}
              placeholder="Nome"
              onChangeText={form.handleChange('firstName')}
              errorMessage={form.errors.firstName}
            />

            <Input
              value={form.values.lastName}
              placeholder="Sobrenome"
              onChangeText={form.handleChange('lastName')}
            />

            {form.values.kind == 'person' && (
              <Input
                value={form.values.cpf}
                placeholder="CPF"
                onChangeText={form.handleChange('cpf')}
                errorMessage={form.errors.cpf}
              />
            )}

            {form.values.kind == 'company' && (
              <View>
                <Input
                  value={form.values.cnpj}
                  placeholder="CNPJ"
                  onChangeText={form.handleChange('cnpj')}
                  errorMessage={form.errors.cnpj}
                />
                <Input
                  value={form.values.companyLegalName}
                  placeholder="Razão Social"
                  onChangeText={form.handleChange('companyLegalName')}
                />
              </View>
            )}

            {this._renderPersonFields(form)}
            {this._renderCompanyFields(form)}

            <Input
              value={form.values.email}
              placeholder="Email"
              onChangeText={form.handleChange('email')}
              keyboardType="email-address"
              errorMessage={form.errors.email}
            />

            <Input
              value={form.values.password}
              placeholder="Senha"
              onChangeText={form.handleChange('password')}
              secureTextEntry
            />

            <Input
              value={form.values.passwordConfirmation}
              placeholder="Confirmação de senha"
              onChangeText={form.handleChange('passwordConfirmation')}
              secureTextEntry
            />

            <Button
              title="Salvar"
              onPress={form.handleSubmit}
              loading={form.isSubmitting}
            />
          </KeyboardAwareScrollView>
        )}
      </Formik>
    );
  }

  _submit = (values, actions) => {
    const FormSchema =
      values.kind == 'person' ? FormPersonSchema : FormCompanySchema;

    FormSchema.validate(values, { abortEarly: false })
      .then(valid => {
        if (valid) this.props.onSubmit(values).then(() => {
          actions.setSubmitting(false);
        })
      })
      .catch(formErrors => {
        const errors = {};

        formErrors.inner &&
          formErrors.inner.forEach(err => {
            errors[err.path] = err.message;
          });

        Alert.alert(
          'Não foi possível continuar',
          'Verifique os erros e tente novamente'
        );

        actions.setSubmitting(false);
        actions.setErrors(errors);
      });
  };

  _renderPersonFields = form => {
    if (form.values.kind == 'company') return;
    return (
      <Input
        value={form.values.cpf}
        placeholder="CPF"
        onChangeText={form.handleChange('cpf')}
        errorMessage={form.errors.cpf}
      />
    );
  };

  _renderCompanyFields = form => {
    if (form.values.kind == 'person') return;
    return (
      <View>
        <Input
          value={form.values.cnpj}
          placeholder="CNPJ"
          onChangeText={form.handleChange('cnpj')}
          errorMessage={form.errors.cnpj}
        />
        <Input
          value={form.values.companyLegalName}
          placeholder="Razão Social"
          onChangeText={form.handleChange('companyLegalName')}
        />
      </View>
    );
  };
}

yup.setLocale({
  mixed: {
    default: 'Não é válido',
    required: 'deve ser preenchido',
  },
  string: {
    email: 'deve ser um email válido!',
  },
});

const FormDefaultSchema = yup.object().shape({
  kind: yup.string().required(),
  firstName: yup.string().required(),
  email: yup
    .string()
    .email()
    .required(),
});

const FormPersonSchema = FormDefaultSchema.concat(
  yup.object().shape({
    cpf: yup.string().required(),
  })
);

const FormCompanySchema = FormDefaultSchema.concat(
  yup.object().shape({
    cnpj: yup.string().required(),
  })
);