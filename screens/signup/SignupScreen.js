import React from 'react';
import gql from 'graphql-tag';
import { Alert } from 'react-native';
import { Mutation } from 'react-apollo';

import SignupForm from './SignupForm';
import Server from '../../constants/Server';

const SignupMutation = gql`
  mutation CursodeReactSignup($input: OfficeRenterSignupInput!) {
    officeRenterSignup(input: $input) {
      errors
      token {
        accessToken
      }
    }
  }
`;

export default class SignupScreen extends React.Component {
  static navigationOptions = {
    title: 'Cadastre-se!',
    headerStyle: {
      backgroundColor: '#0a5a8e',
    },
    headerTintColor: 'white',
  };

  render() {
    return (
      <Mutation mutation={SignupMutation}>
        {mutate => (
          <SignupForm
            onSubmit={values => this._tryToSignup(mutate, values)}
          />
        )}
      </Mutation>
    );
  }

  _tryToSignup = (mutate, formValues) => {
    const input = {
      ...formValues,
      clientId: Server.clientId,
      clientSecret: Server.clientSecret,
    };

    return mutate({ variables: { input } })
      .then(r => {
        console.log('r', r);
      })
      .catch(err => {
        Alert.alert(err.message);
      });
  };
}