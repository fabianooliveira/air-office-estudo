import React from 'react';
import { View, Text, ActivityIndicator, AsyncStorage } from 'react-native';

export default class LoadingScreen extends React.Component {
    componentDidMount() { // Chama depois que a tela for montada -- ON START OU VIEWDIDAPPEAR
        AsyncStorage.getItem('AccessToken').then(token => {
            if (token) {
                this.props.navigation.navigate('Private', {accessToken: token});
            } else {
                this.props.navigation.navigate('Login');
            }
        })
    }

    render() { // MONTAR A TELA
        return (
            <View style={{ flex: 1, justifyContent: "center", alignContent: "center" }}>
                
            </View>)
    }
}