import React from 'react';
import {
    createStackNavigator,
    createAppContainer,
} from 'react-navigation';

import HomeForm from '../screens/home/HomeForm';
import JogoDaVelha from '../screens/home/JogoDaVelha';
import Upload from '../screens/home/Upload';
import ChatR from '../screens/home/ChatRooms';
import Maps from '../screens/home/Maps';

import { ApolloProvider } from 'react-apollo';
import { PrivateAPIClient } from '../constants/ApolloClient';
import Listar from '../screens/home/Listar';
import NotificationHandler from '../components/NotificationHandler';
import ChatRooms from '../screens/home/ChatRooms';

const Navigator = createAppContainer(
    createStackNavigator(
        {
            Home: HomeForm,
            JogoVelha: JogoDaVelha,
            List: Listar,
            Up: Upload,
            ChatR: ChatRooms,
            Map: Maps
        },
        {
            initialRouteName: 'Home',
        }
    )
);

export default class PrivateStackNavigator extends React.Component {
    static router = Navigator.router;
    render() {
        const token = this.props.navigation.getParam('accessToken');
        console.log(token)
        const privateAPIClient = PrivateAPIClient(token);
        return (
            <ApolloProvider client={privateAPIClient}>
                <Navigator navigation={this.props.navigation} />
                <NotificationHandler/>
            </ApolloProvider>
        );
    }
}