import {
  createAppContainer,
  createSwitchNavigator,
} from 'react-navigation';

import PrivateStackNavigator from './PrivateStackNavigator';
import LoadingScreen from '../screens/LoadingScreen';
import AuthStackNavigator from './AuthStackNavigator';

export default createAppContainer(createSwitchNavigator(
  {
    Private: PrivateStackNavigator,
    Auth: AuthStackNavigator,
    Loading: LoadingScreen,
  },
  {
    initialRouteName: 'Loading',
  }
));


