import React from 'react';
import {
  createStackNavigator,
  createAppContainer,
} from 'react-navigation';

import LoginScreen from '../screens/login/LoginScreen';
import SignupScreen from '../screens/signup/SignupScreen';
import LoadingScreen from '../screens/LoadingScreen';

import { ApolloProvider } from 'react-apollo';
import { PublicAPIClient } from '../constants/ApolloClient';

const Navigator = createAppContainer(
  createStackNavigator(
    {
      Loading: LoadingScreen,
      Login: LoginScreen,
      Signup: SignupScreen,
    },
    {
      initialRouteName: 'Login',
    }
  )
);

export default class AuthStackNavigator extends React.Component {
  static router = Navigator.router;
  render() {
    return (
      <ApolloProvider client={PublicAPIClient}>
        <Navigator navigation={this.props.navigation} />
      </ApolloProvider>
    );
  }
}