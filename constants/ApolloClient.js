import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import Server from './Server';

export const PublicAPIClient = new ApolloClient({
  link: createHttpLink({ uri: `${Server.url}/graphql` }),
  cache: new InMemoryCache(),
});

export const PrivateAPIClient = (userToken) => {
  const authLink = createHttpLink({
    uri: `${Server.url}/office_renters/graphql.json`,
    headers: { Authorization: `Bearer ${userToken}` },
  });

  return new ApolloClient({
    link: authLink,
    cache: new InMemoryCache(),
  });
};
