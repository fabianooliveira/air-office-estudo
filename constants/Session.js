import { AsyncStorage } from 'react-native';

export const signIn = async userToken => {
 await AsyncStorage.setItem('AccessToken', userToken);
};

export const getToken = async () => {
 return await AsyncStorage.getItem('AccessToken')
};

export const signOut = AsyncStorage.clear