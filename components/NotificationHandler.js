import React from 'react'
import { Notifications } from 'expo'
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import * as Permissions from 'expo-permissions'

const SaveDeviceTokenMutation = gql`
  mutation MobileSaveDeviceToken($token: String!) {
    saveDeviceToken(input: { token: $token }) {
      device {
        id
        token
      }
      errors
    }
  }
`;

class NotificationHandler extends React.Component {
    componentDidMount() {
        this._askPermission()
    }

    

    _askPermission = () => {
        Permissions.askAsync(Permissions.NOTIFICATIONS).then(permission => {
            if (permission.status == 'granted') {
                this._getPushNotificationToken()
            }
        })
    };

    _getPushNotificationToken = () => {
        Notifications.getExpoPushTokenAsync().then(token => {
            this._saveTokenDevie(token)
        })
    }


    _saveTokenDevie = (token) => {
        console.log(token + "      asduyasdiygadsgyadsugyaYUGASgyuadsguydasuygadsugydsaguydsaguydasugyadsguydasugygyadsaS")
        this.props.mutate({ variables: { token: token }}).then( response => {
            console.log(response)
        })
    }

    render() {
        return null
    }
}

export default () => (
    <Mutation mutation={SaveDeviceTokenMutation}>
        {mutate => <NotificationHandler mutate={mutate} />}
    </Mutation>
);