import React from 'react';
import { Button } from 'react-native';

const onPressLearnMore = (msg) => {
  alert(msg);
}

const AlertButton = props => (
  <Button
    onPress={onPressLearnMore(props.alertMsg)}
    title={props.titulo}
    color="#841584" />
);

export default AlertButton;