import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

export default props => {
    return (
        <TouchableOpacity onPress={props.onPress} style={[css.wrap, props.style]}>
            <Text style={css.text}> {props.title} </Text>
        </TouchableOpacity>
    );
}

const css = StyleSheet.create({
    wrap: {
        padding: 10
    },
    text: {
        color: 'white'
    }
});